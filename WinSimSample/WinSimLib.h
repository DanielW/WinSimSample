#ifndef WINSIMLIB_H
#define WINSIMLIB_H

#ifdef _WINDLL
#define WINMAIN int WINSIMDLL_API WinSimRun
#else
#define WINMAIN int APIENTRY _tWinMain
#endif

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' \
version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include <Windows.h>
#define RGBA(r, g ,b, a)  ((DWORD) ((((BYTE) (b) | ((WORD) (g) << 8)) | (((DWORD) (BYTE) (r)) << 16)) | (((DWORD) (BYTE) (a)) << 24)))
#define RGB(r, g ,b)  ((DWORD) ((((BYTE) (b) | ((WORD) (g) << 8)) | (((DWORD) (BYTE) (r)) << 16)) | (((DWORD) (BYTE) (255)) << 24)))

#define COLOR_RED 0xffff0000
#define COLOR_GREEN 0xff00ff00
#define COLOR_BLUE 0xff0000ff
#define COLOR_CYAN 0xff00ffff
#define COLOR_MAGENTA 0xffff00ff
#define COLOR_YELLOW 0xffffff00
#define COLOR_WHITE 0xffffffff
#define COLOR_GRAY 0xff7f7f7f
#define COLOR_BLACK 0xff000000


#ifdef WINSIMDLL_EXPORTS
#define WINSIMDLL_API __declspec(dllexport) 
#else
#define WINSIMDLL_API __declspec(dllimport) 
#endif

typedef void(*SIMMAIN)(void);
typedef void(*SIMRESET)(void);
#define WINSIM_ID DWORD
#define WINSIM_DEF_ID (WINSIM_ID)nullptr

extern "C"
{
	int WINSIMDLL_API WinSimRun(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow);


	/// <summary>Funkcja inicjalizuj�ca niezb�dne biblioteki. Nale�y wywo�a� j� zaraz po uruchomieniu programu, przed wywo�aniem pozosta�ych funkcji z biblioteki WinSimLib.</summary>
	void WINSIMDLL_API WinSimStartup();

	/// <summary>Inicjalizuje symulacj�.</summary>
	///	<param name="SimMain">Adres funkcji przeprowadzaj�cej symulacj�.</param>
	///	<param name="SimTime">Wska�nik do zmiennej reprezentuj�cej czas w symulacji.</param>
	///	<param name="onReset">Adres funkcji, kt�ra ma zosta� wywo�ana podczas resetowania symulacji (np. definiuj�ca warunki pocz�tkowe symulacji).</param>
	void WINSIMDLL_API WinSimInit(SIMMAIN SimMain, double *SimTime, SIMRESET onReset = NULL);

	/// <summary>Tworzy okno z przebiegami.</summary>
	///	<param name="lpWindowName">Ci�g znak�w opisuj�cy tytu� okna na pasku okna</param>
	///	<param name="nWidth">Szeroko�� okna w pixelach.</param>
	///	<param name="nHeight">Wysoko�� okna w pixelach. Wysoko�� uwzgl�dnia tak�e pasek tytu�owy, zatem obszar roboczy jest mniejszy ni� <paramref name="nHeight" /></param>
	/// <returns>Zwraca identyfikator okna</returns>
	WINSIMDLL_API WINSIM_ID WinSimCreateChartWindow(TCHAR *lpWindowName, int nWidth, int nHeight);

	/// <summary>Dodaje wykres do okna z przebiegami.</summary>
	///	<param name="windowID">Identyfikator okna, do kt�rego ma by� dodany wykres. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnie wykres dodawany jest do ostatnio utworzonego okna.</param>
	/// <returns>Zwraca identyfikator wykresu</returns>
	WINSIMDLL_API WINSIM_ID WinSimAddChart(WINSIM_ID windowID = WINSIM_DEF_ID);

	/// <summary>Dodaje zmienn� do rejestracji. Rejestrowane pr�bki dodawane b�d� do przebiegu podczas wywo�aniu funkcji <see cref="WinSimTick()"/></summary>
	///	<param name="x">Wska�nik do wsp�rz�dnej poziomej rejestrowanych punkt�w.</param>
	///	<param name="y">Wska�nik do wsp�rz�dnej pionowej rejestrowanych punkt�w.</param>
	///	<param name="name">Nazwa zmiennej, kt�ra wy�wietlana b�dzie na legendzie.</param>
	///	<param name="color">Kolor wy�wietlanego przebiegu. Skorzysta� mo�na z predefiniowanych warto�ci (np. COLOR_RED) lub z makra RGB(r,g,b).</param>
	///	<param name="offset">Przesuni�cie przebiegu w pionie o dan� warto��. Parametr opcjonalny.</param>
	///	<param name="scale">Skalowanie wykresu. Parametr opcjonalny.</param>
	///	<param name="chartID">Identyfikator wykresu do kt�rego zostanie dodany przebieg. Parametr opcjonalny - domy�lnie przebieg dodawany jest do ostatnio utworzonego wykresu.</param>
	/// <returns>Zwraca identyfikator sygna�u</returns>
	WINSIMDLL_API WINSIM_ID WinSimAddSignalXY(const double *x, const double *y, TCHAR *name, DWORD color, double offset = 0.0, double scale = 1.0, WINSIM_ID windowID = WINSIM_DEF_ID, WINSIM_ID chartID = WINSIM_DEF_ID);

	/// <summary>Dodaje zmienn� do rejestracji. Rejestrowane pr�bki dodawane b�d� do przebiegu podczas wywo�aniu funkcji <see cref="WinSimTick()"/></summary>
	///	<param name="y">Wska�nik do wsp�rz�dnej pionowej rejestrowanych punkt�w.</param>
	///	<param name="name">Nazwa zmiennej, kt�ra wy�wietlana b�dzie na legendzie.</param>
	///	<param name="color">Kolor wy�wietlanego przebiegu. Skorzysta� mo�na z predefiniowanych warto�ci (np. COLOR_RED) lub z makra <paramref name="RGB" />.</param>
	///	<param name="offset">Przesuni�cie przebiegu w pionie o dan� warto��. Parametr opcjonalny.</param>
	///	<param name="scale">Skalowanie wykresu. Parametr opcjonalny.</param>
	///	<param name="chartID">Identyfikator wykresu do kt�rego zostanie dodany przebieg. Parametr opcjonalny - domy�lnie przebieg dodawany jest do ostatnio utworzonego wykresu.</param>
	/// <returns>Zwraca identyfikator sygna�u</returns>
	WINSIMDLL_API WINSIM_ID WinSimAddSignal(const double *y, TCHAR *name, DWORD color, double offset = 0.0, double scale = 1.0, WINSIM_ID chartID = WINSIM_DEF_ID);

	/// <summary>Ustawia pocz�tkow� poziom� skal� wykres�w w danym oknie.</summary>
	///	<param name="width">Szeroko�� wykresu (w jednostce zmiennej X).</param>
	///	<param name="windowID">Identyfikator okna. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnym oknem jest ostatnio utworzone okno.</param>
	void WINSIMDLL_API WinSimSetChartsWidth(double width, WINSIM_ID windowID = WINSIM_DEF_ID);

	/// <summary>Ustawia pocz�tkowe poziome po�o�enie wykres�w w danym oknie.</summary>
	///	<param name="x0">Warto�� zmiennej X na lewej kraw�dzi wykres�w.</param>
	///	<param name="windowID">Identyfikator okna. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnym oknem jest ostatnio utworzone okno.</param>
	void WINSIMDLL_API WinSimSetChartsX0(double x0, WINSIM_ID windowID = WINSIM_DEF_ID);

	/// <summary>Ustawia pocz�tkow� pionow� skal� wykresu.</summary>
	///	<param name="height">Wysoko�� wykresu (w jednostce zmiennej Y).</param>
	///	<param name="windowID">Identyfikator okna. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnym oknem jest ostatnio utworzone okno.</param>
	void WINSIMDLL_API WinSimSetChartHeight(double height, WINSIM_ID chartID = WINSIM_DEF_ID);

	/// <summary>Ustawia pocz�tkowe pionowe po�o�enie wykresu.</summary>
	///	<param name="x0">Warto�� zmiennej Y na dolnej kraw�dzi wykresu.</param>
	///	<param name="windowID">Identyfikator okna. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnym oknem jest ostatnio utworzone okno.</param>
	void WINSIMDLL_API WinSimSetChartY0(double y0, WINSIM_ID chartID = WINSIM_DEF_ID);

	/// <summary>Definiuje kolory okna. Kolory tworzone mog� by� przy u�yciu makra RGB(r,g,b).</summary>
	///	<param name="background">Kolor t�a.</param>
	///	<param name="grid">Kolor siatki.</param>
	///	<param name="labels">Kolor etykiet.</param>
	///	<param name="windowID">Identyfikator okna. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnym oknem jest ostatnio utworzone okno.</param>
	void WINSIMDLL_API WinSimSetWindowColors(DWORD background, DWORD grid, DWORD labels, WINSIM_ID windowID = WINSIM_DEF_ID);

	/// <summary>Tworzy okno z parametrami.</summary>
	///	<param name="lpWindowName">Nazwa okna.</param>
	///	<param name="nWidth">Szeroko�� okna.</param>
	///	<param name="nHeight">Wysoko�� okna.</param>
	/// <returns>Zwraca identyfikator okna</returns>
	WINSIMDLL_API WINSIM_ID WinSimCreatePropWindow(TCHAR *lpWindowName, int nWidth, int nHeight);

	/// <summary>Dodaje nowy parametr.</summary>
	///	<param name="pointer">Wska�nik zmiennej.</param>
	///	<param name="name">Nazwa parametru.</param>
	///	<param name="shortcut">Skr�t parametru - tylko litery. Naci�ni�cie skr�tu w oknie wykres�w powoduje przej�cie do okna parametr�w oraz zaznaczenie warto�ci parametru przypisanego do skr�tu. Naci�ni�cie klawisza Enter powoduje powr�t do okna przebieg�w.</param>
	///	<param name="sliderMin">Warto�� minimalna suwaka.</param>
	///	<param name="sliderMax">Warto�� maksymalna suwaka.</param>
	///	<param name="windowID">Identyfikator okna parametr�w. Identyfikator jest zwracany podczas tworzenia okna. Parametr opcjonalny - domy�lnym oknem jest ostatnio utworzone okno.</param>
	/// <returns>Zwraca identyfikator parametru</returns>
	WINSIMDLL_API WINSIM_ID WinSimAddProp(double *pointer, TCHAR *name, char shortcut = NULL, double sliderMin = -2, double sliderMax = 2, WINSIM_ID windowID = WINSIM_DEF_ID);

	/// <summary>Ustawia krok rejestracji przebieg�w oraz maksymalny czas symulacji. Parametry te maj� wp�yw na ilo�� przydzielanej pami�ci potrzebnej do zapisu przebieg�w. Funkcj� nale�y wywo�a� po dodaniu wszystkich rejestrowanych sygna��w za pomoc� funkcji <paramref name="WinSimAddSignal" />.</summary>
	///	<param name="step">Krok rejestracji.</param>
	///	<param name="simulationTime">Maksymalny czas symulacji.</param>
	void WINSIMDLL_API WinSimSetStep(double step, double simulationTime);

	/// <summary>Aktywuje okno konsoli. Nale�y wywo�a� przed wywo�aniem <paramref name="WinSimRun" />.</summary>
	///	<param name="state">1 - konsola w��czona, 0 - konsola wy��czona</param>
//	void WINSIMDLL_API WinSimTakeConsoleBack(FILE *in, FILE *out);
}

#endif //WINSIMLIB_H