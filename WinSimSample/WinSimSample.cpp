#include "WinSimLib.h"
#include "sim.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	WINSIM_ID poleWykresu1, poleWykresu2;

	WinSimStartup();

	//KONFIGURACJA OKIEN WYKRESÓW
	WinSimCreateChartWindow(_T("Nazwa okna"), 1280, 720);
	poleWykresu1 = WinSimAddChart();
	WinSimSetChartY0(-0.5);
	poleWykresu2 = WinSimAddChart();
	WinSimSetChartHeight(4);
	WinSimSetChartY0(-2.0);
	WinSimAddSignal(&x1, _T("Przebieg 1"), COLOR_GREEN, -1.0, 5.0);
	WinSimAddSignal(&x1, _T("Przebieg 2"), RGB(255, 126, 255));
	WinSimAddSignal(&x2, _T("Przebieg 3"), COLOR_RED, 0, 1, poleWykresu1);
	WinSimSetWindowColors(COLOR_WHITE, COLOR_GRAY, COLOR_BLACK);
	WinSimSetChartsWidth(20.0);

	//KONFIGURACJA OKIEN PARAMETRÓW
	WinSimCreatePropWindow(_T("Nazwa okna z parametrami"), 430, 800);
	WinSimAddProp(&omega, _T("w"), 'w');
	WinSimAddProp(&K, _T("k"), 'k', -2.0, 2.0);
	WinSimAddProp(&dumping, _T("z"), NULL, -2, 2);
	WinSimAddProp(&u, _T("u"), NULL, -5, 5);

	//KONFIGURACJA PARAMETRÓW SYMULACJI
	WinSimInit(SimMain, &t, onReset);
	WinSimSetStep(stepTime, 200.0);

	WinSimRun(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	return 0;
}
