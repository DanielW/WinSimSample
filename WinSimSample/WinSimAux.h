//#include <windows.h>

#ifdef WINSIMDLL_EXPORTS
#define WINSIMDLL_API __declspec(dllexport) 
#else
#define WINSIMDLL_API __declspec(dllimport) 
#endif

// C RunTime Header Files
#define _USE_MATH_DEFINES
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <math.h>
#include <iostream>
#include <process.h>
#include <time.h>


extern "C" int WINSIMDLL_API WinSimTick();