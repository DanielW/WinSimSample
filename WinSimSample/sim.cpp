#include "sim.h"

double t;
double stepTime = 0.001;

double x1, x2;	//zmienne
double omega = 1, K = 0.5, dumping = 0.5;	//parametry
double u = 1;	//wejscia

void SimMain()
{
	while (WinSimTick())
	{
		CzlonOscylacyjny();
		t += stepTime;
	}
}

void onReset()
{
	x1 = 0;
	x2 = 0;
}

void CzlonOscylacyjny()
{
	x1 += stepTime*(x2);
	x2 += stepTime*(-1/omega/omega*x1-2.0*dumping*x2+K/omega/omega*u);
}

